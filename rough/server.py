import json
import secrets
from flask import Flask, request, Response, abort

app = Flask(__name__)

@app.route('/send', methods=['POST'])
def send():

    try:
        name = request.form['name']
        msg = request.form['msg']
    except:
        return abort(400)

    with open('msgs.json', 'r') as f:
        data = json.load(f)
    with open('msgs.json', 'w') as f:
        data[secrets.token_hex(32)] = {
            "name" : name,
            "msg" : msg,
            "ip" : request.remote_addr
        }
        json.dump(data, f, indent=4)
    return msg

@app.route('/getdata', methods=['GET'])
def get():
    with open('msgs.json', 'r') as f:
        data = json.load(f)
    
    resp = Response(json.dumps(data))
    resp.headers['Access-Control-Allow-Origin'] = '*'
    return resp

@app.route('/online')
def online():
    resp = Response("")
    resp.headers['Access-Control-Allow-Origin'] = '*'
    return resp

if __name__ == '__main__':
    app.run('0.0.0.0', port=8000, debug=True)
