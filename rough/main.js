// Disregard. Just for cookies
function createCookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    }
    else var expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
}
function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}
// Code starts here
const IP = 'http://10.210.181.58:8000'
// Keeps track of which msgs are already on the screen
var recived_msgs = [];

function fetch_msgs() {
    //Check if online
    fetch(IP + "/online")
        .then(response => {
            if (response.ok) {
                $("#server_status").text('Online');
                $("#server_status").css("color", "green")
            }
        })
        .catch(err => {
            $("#server_status").text('Offline');
            $("#server_status").css("color", "red")
            return null;
        });
    fetch(IP + "/getdata")
        // parse to json
        .then(response => response.json())
        .then(msgs => {

            for (let [key, data] of Object.entries(msgs)) {
                // if the message isnt already written to the screen then write it
                if ($.inArray(key, recived_msgs) < 0) {
                    $('#response').append("<br> " + data["name"] + ': ' + data["msg"]);
                    recived_msgs.push(key);
                } else {// Do nothing}
                }

            };
        }
        )

}
$("#get").click(fetch_msgs);

$('#send').click(function () {
    $.post(IP + "/send", {
        msg: $('#msg').val(),
        name: readCookie("name")
    });
    $("#msg").val('');
})

$('#set_name').click(function () {
    let name = $("#name_inp");
    createCookie("name", name.val());
    $("#name").html(name.val());
    alert('Name has been set to ' + name.val());
})

$("#clear").click(function () {
    $("#response").html('');
})

// For sending messages by pressing enter
$('#msg').keypress((e) => {
    // Enter key corresponds to number 13
    if (e.which === 13) {
        $('#send').click();
    }
})

// Set name automaticlly if they already have set it in previous session
if (readCookie("name")) {
    $("#name").html(readCookie("name"));
}

async function main() {
    while (true) {
        fetch_msgs();
        // sleep for 400 ms
        await new Promise(r => setTimeout(r, 400));
    }
}

main();
