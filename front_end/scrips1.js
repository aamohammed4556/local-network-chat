var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;

function make_request(){
    httpRequest = new XMLHttpRequest();
    httpRequest.onreadystatechange = go;
    httpRequest.open('GET', 'http://localhost:8080/get', true);
    httpRequest.send();
}

function go() {
    if (httpRequest.readyState === XMLHttpRequest.DONE) {
      if (httpRequest.status === 200) {
        console.log(httpRequest.responseText);
      } else {
        console.log('There was a problem with the request.');
      }
    }
}

make_request()