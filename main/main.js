// Disregard. Just for cookies
function createCookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    }
    else var expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
}
function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}
// Code starts here 
const IP = 'http://10.210.181.58:8000'
// Keeps track of which msgs are already on the screen
var recived_ids = [];
let unread = 0;

const scrollToBottom = () => {
    $("#messages").animate({
        scrollTop: $("#messages").prop('scrollHeight')
    }, 10);
}

function updateTitle(unread) {
    document.title = "Chat Room (" + unread + ")"; 
}

function putMsg(msg) {
    // put message on chat
    $('#messages').append("<bold>" + msg["name"] + '</bold>: ' + msg["msg"] + "<hr>");
}

// function to sort the ids in the msgs list
function SortById(a, b){
  var aID = a.id;
  var bID = b.id;
  return ((aID < bID) ? -1 : ((aID > bID) ? 1 : 0));
}

async function getMsg(id) {
    return await new Promise((resolve, rej) => {
        fetch(IP + "/msg/" + id)
        .then(msg => resolve(msg.json()));
    });
}

async function getAllIds() {
    return await new Promise((resolve, rej) => {
        fetch(IP + "/msg")
        .then(ids => resolve(ids.json()));
    });
}

async function initMsgs() {
    let msgs = [];
    let ids = await getAllIds();
    // The whole point of doing this like this is to avoid the messages being put out of order by the fetch API
    // returning values too late and messing up data
    for (var id of ids) {
        let msg = await getMsg(id);
        msgs.push(msg);
        recived_ids.push(id);
    }
    // sort the msgs by their ids
    msgs.sort(SortById);
    msgs.forEach(msg => {
        putMsg(msg);
        scrollToBottom();
    });

}

async function fetch_msgs() {
    // Get list of all msgs
    let ids = await getAllIds();
    ids.forEach(id => {
        if ($.inArray(id, recived_ids) > -1) {
            return null;
        }
        getMsg(id)
        .then(msg => {
            recived_ids.push(id);
            putMsg(msg);
            unread++;
            scrollToBottom();
        });

    })
}

$('#send').click(function () {
    $.post(IP + "/msg", {
        msg: $('#msg').val(),
        name: readCookie("name")
    }
    );
    $('#msg').val('');
})

$('#set_name').click(function () {
    let name = $("#name_inp");
    createCookie("name", name.val());
    $("#name").html(name.val());
    alert('Name has been set to ' + name.val());
})

// Set name automaticlly if they already have set it in previous session
if (readCookie("name")) {
    $("#name").html(readCookie("name"));
}

// For sending messages by pressing enter
$('#msg').keypress((e) => {
    // Enter key corresponds to number 13
    if (e.which === 13) {
        $('#send').click();
        $('#msg').val('');
    }
})


async function main() {
    await initMsgs();
    while (true) {
        fetch_msgs();
        if (!document.hidden) {
            unread = 0;
        }
        updateTitle(unread);
        // sleep for 500 ms
        await new Promise(r => setTimeout(r, 500));
    }
}

main();