import time
import json
import secrets
from flask import Flask, request, Response, render_template, escape
from flask_restful import Api, Resource, abort

app = Flask(__name__)
api = Api(app)


def get_data():
    """Return all messages"""
    with open('data.json', 'r') as database:
        return json.load(database)

def save_data(data):
    global id
    with open('data.json', 'w') as database:
        json.dump(data, database, indent=2)

class Messages(Resource):
    def get(self, id):
        data = get_data()
        if id in data:
            resp = Response(json.dumps(data[id]))
            resp.headers['Access-Control-Allow-Origin'] = '*'
            return resp
        abort(404)

    def delete(self, id):
        pass

class FullMessages(Resource):
    def __init__(self) -> None:
        super().__init__()

    def get(self):
        data = list(get_data().keys())
        if len(data) == 0:
            data = []
        resp = Response(json.dumps(data))
        resp.headers['Access-Control-Allow-Origin'] = '*'
        return resp 

    def post(self):
        global id
        req_args = ["name", "msg"]
        for arg in req_args: 
            if arg not in request.form:
                abort(401)

        data = get_data()
        id += 1
        data[id] = {
            'ip' : request.remote_addr,
            'msg' : escape(request.form['msg'][:200]),
            'name' : escape(request.form['name'][:2000]),
            'date' : time.time(),
            'id' : id
        }
        save_data(data)
        return '', 201


api.add_resource(Messages, '/msg/<id>')
api.add_resource(FullMessages, '/msg')

if __name__ == "__main__":
    data = get_data()
    id = 0 if len(data.keys()) == 0 else max(list(map(int, list(data.keys()))))
    app.run('0.0.0.0', 8000, debug=True)

























